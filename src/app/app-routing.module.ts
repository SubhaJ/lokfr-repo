import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { ProfessionalsComponent } from './professionals/professionals.component';

const routes: Routes = [
  {path: '', component: HomeComponent },
  {path: 'home', pathMatch:'full',component: HomeComponent },
  {path: 'loginpage', component:FooterComponent},
  {path: 'professioals', component:ProfessionalsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
