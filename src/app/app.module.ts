import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { WelcomeBannarComponent } from './home/welcome-bannar/welcome-bannar.component';
import { ServicesTabComponent } from './home/services-tab/services-tab.component';
import { TryServicesTabComponent } from './home/try-services-tab/try-services-tab.component';
import { OffersTabComponent } from './home/offers-tab/offers-tab.component';
import { SupportTabComponent } from './home/support-tab/support-tab.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProfessionalsComponent } from './professionals/professionals.component';
import { MaterialModule  }  from './material/material.module';
import { DialogCopoComponent } from './professionals/dialog-copo/dialog-copo.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    WelcomeBannarComponent,
    ServicesTabComponent,
    TryServicesTabComponent,
    OffersTabComponent,
    SupportTabComponent,
    ProfessionalsComponent,
    DialogCopoComponent
  ],
  entryComponents:[DialogCopoComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
