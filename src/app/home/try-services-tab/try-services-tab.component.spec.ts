import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TryServicesTabComponent } from './try-services-tab.component';

describe('TryServicesTabComponent', () => {
  let component: TryServicesTabComponent;
  let fixture: ComponentFixture<TryServicesTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TryServicesTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TryServicesTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
