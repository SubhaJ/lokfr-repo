import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WelcomeBannarComponent } from './welcome-bannar.component';

describe('WelcomeBannarComponent', () => {
  let component: WelcomeBannarComponent;
  let fixture: ComponentFixture<WelcomeBannarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WelcomeBannarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomeBannarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
