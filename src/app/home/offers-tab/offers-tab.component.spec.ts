import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OffersTabComponent } from './offers-tab.component';

describe('OffersTabComponent', () => {
  let component: OffersTabComponent;
  let fixture: ComponentFixture<OffersTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OffersTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OffersTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
