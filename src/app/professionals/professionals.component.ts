import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog'
import { DialogCopoComponent } from './dialog-copo/dialog-copo.component';
@Component({
  selector: 'app-professionals',
  templateUrl: './professionals.component.html',
  styleUrls: ['./professionals.component.scss']
})
export class ProfessionalsComponent implements OnInit {

  constructor(public dialg: MatDialog) { }
  opendialog(){
    this.dialg.open(DialogCopoComponent)
  }
  ngOnInit(): void {
  }

}
