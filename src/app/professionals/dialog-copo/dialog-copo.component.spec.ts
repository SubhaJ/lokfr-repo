import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogCopoComponent } from './dialog-copo.component';

describe('DialogCopoComponent', () => {
  let component: DialogCopoComponent;
  let fixture: ComponentFixture<DialogCopoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogCopoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogCopoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
